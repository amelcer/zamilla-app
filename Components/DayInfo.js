import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { dateToDay, parseDate } from "../Helpers/dates";
import FreeDay from "./FreeDay";

export default function DayInfo(props) {
    const { date, leavingRooms, arrivals } = props;

    if (leavingRooms.length === 0 && arrivals.length === 0) {
        return <FreeDay date={date} />;
    }

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.font}> {`${dateToDay(date)} ${parseDate(date)}`} </Text>
            </View>
            <View style={styles.panels}>
                <View style={styles.arrivals}>
                    <Text style={styles.font}> Wyjazdy: </Text>
                    <Text style={styles.font}> {leavingRooms && leavingRooms.map((room) => `${room}, `)} </Text>
                    <View style={styles.line}></View>
                    {leavingRooms && <Text style={styles.font}> Suma: {leavingRooms.length}</Text>}
                </View>
                <View style={styles.leaves}>
                    <Text style={styles.font}> Przyjazdy: </Text>
                    <Text style={styles.font}> {arrivals && arrivals.map((room) => `${room}, `)} </Text>
                    <View style={styles.line}></View>
                    {arrivals && <Text style={styles.font}>Suma: {arrivals.length}</Text>}
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        display: "flex",
        flexDirection: "column",
        marginBottom: 10,
    },
    header: {
        width: "100%",
        alignItems: "center",
        backgroundColor: "#4e4e4f",
        padding: 10,
    },
    panels: {
        display: "flex",
        flexDirection: "row",
        width: "100%",
    },
    arrivals: {
        width: "50%",
        alignItems: "center",
        backgroundColor: "#f24f4f",
        flexDirection: "column",
        padding: 10,
    },
    leaves: {
        width: "50%",
        alignItems: "center",
        backgroundColor: "#5487f7",
        flexDirection: "column",
        padding: 10,
    },
    line: {
        width: "80%",
        backgroundColor: "black",
        height: 1,
        marginTop: "5%",
        marginBottom: "5%",
    },
    font: {
        color: "white",
    },
});
