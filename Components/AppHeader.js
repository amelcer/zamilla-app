import React from "react";
import { StyleSheet } from "react-native";
import { Container, Header, Left, Body, Right, Button, Icon, Title } from "native-base";

export default function AppHeader(props) {
    const { title, left, right } = props;

    const handleForward = () => {
        right();
    };

    const handleBackward = () => {
        left();
    };

    return (
        <Header style={styles.header}>
            <Left>
                <Button transparent onPress={handleBackward}>
                    <Icon name="arrow-back" />
                </Button>
            </Left>
            <Body>
                <Title> {title} </Title>
            </Body>
            <Right>
                <Button transparent onPress={handleForward}>
                    <Icon name="arrow-forward" />
                </Button>
            </Right>
        </Header>
    );
}

const styles = StyleSheet.create({
    header: {
        height: 100,
        alignItems: "center",
    },
});
