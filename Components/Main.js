import React, { useState, useEffect } from "react";
import { Container, Content, Spinner, Text } from "native-base";
import AppHeader from "./AppHeader";
import DayInfo from "./DayInfo";
import * as DateHelper from "../Helpers/dates";
import shortid from "shortid";

export default function Main() {
    const [date, setDate] = useState(new Date());
    const [roomsInfo, setRoomsInfo] = useState([]);
    const [title, setTitle] = useState(DateHelper.parseDate(date));
    const [dataLoaded, setDataLoaded] = useState(false);
    const [dateRange, setDateRange] = useState([]);

    useEffect(() => {
        const dtRange = DateHelper.getDatesRange(date, 4);
        const dtRangeTime = dtRange.map((dt) => dt.getTime());
        //console.log(dtRange);
        const URL = `http://46.41.136.29/rooms/getRooms?from=${dtRange[0].toISOString()}&to=${dtRange[3].toISOString()}`;
        fetch(URL, {
            headers: {
                key: "4398f96c0bdbbee98d60a11337487956:39d9f03b420847da092308aa78f4b6878b573275d04745",
            },
        })
            .then((x) => x.json())
            .then(({ response }) => {
                setDataLoaded(true);
                if (response.length !== dtRange.length) {
                    const responseDates = response.map((rs) => new Date(rs.date).getTime());
                    const freeDates = dtRangeTime
                        .filter((dt) => !responseDates.includes(dt))
                        .map((dt) =>
                            Object.assign({
                                _id: shortid.generate(),
                                date: new Date(dt),
                                arrivingRooms: [],
                                leavingRooms: [],
                            })
                        );
                    const x = freeDates.concat(response).sort((a, b) => new Date(a.date) - new Date(b.date));
                    setRoomsInfo(x);
                    return;
                }
                setRoomsInfo(response);
            });
    }, [date]);

    const nextDate = () => {
        const nextDay = DateHelper.getNextDate(date);
        setDate(nextDay);
        setTitle(DateHelper.parseDate(nextDay));
    };

    const previousDate = () => {
        const prev = DateHelper.getPreviousDate(date);
        setDate(prev);
        setTitle(DateHelper.parseDate(prev));
    };
    return (
        <Container>
            <AppHeader title={title} left={previousDate} right={nextDate} />
            <Content style={{ padding: 10 }}>
                {!dataLoaded ? (
                    <Spinner color="blue" />
                ) : roomsInfo.length === 0 ? (
                    <Text> Brak danych w bazie </Text>
                ) : (
                    <>
                        {roomsInfo.map((lv) => {
                            return (
                                <DayInfo
                                    date={new Date(lv.date)}
                                    leavingRooms={lv.leavingRooms}
                                    arrivals={lv.arrivingRooms}
                                    key={lv._id}
                                />
                            );
                        })}
                    </>
                )}
            </Content>
        </Container>
    );
}
