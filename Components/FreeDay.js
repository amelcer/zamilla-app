import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { dateToDay, parseDate } from "../Helpers/dates";

export default function FreeDay(props) {
    const { date } = props;
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.font}> {`${dateToDay(date)} ${parseDate(date)}`} </Text>
            </View>
            <View style={styles.panels}>
                <View style={styles.content}>
                    <Text style={styles.fontBig}> WOLNE </Text>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        display: "flex",
        flexDirection: "column",
        marginBottom: 10,
    },
    header: {
        width: "100%",
        alignItems: "center",
        backgroundColor: "#4e4e4f",
        padding: 10,
    },
    panels: {
        display: "flex",
        flexDirection: "row",
        width: "100%",
    },
    content: {
        width: "100%",
        height: 90,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#2cdd04",
        flexDirection: "column",
        padding: 10,
    },
    font: {
        color: "white",
    },
    fontBig: {
        color: "white",
        fontSize: 24,
    },
});
