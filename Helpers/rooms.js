export async function getLeavings(dateRange) {
    try {
        const response = await fetch(`http://46.41.136.29/rooms/getRooms?from=${dateRange[0]}&to=${dateRange[3]}`, {
            headers: {
                key: "4398f96c0bdbbee98d60a11337487956:39d9f03b420847da092308aa78f4b6878b573275d04745",
            },
        });
        return response.json();
    } catch (e) {
        return Object.assign({ error: e });
    }

    return null;
}
