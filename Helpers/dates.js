const daysMap = ["Niedziela", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota"];

export function parseDate(date) {
    const day = date.getDate() < 10 ? `0${date.getDate()}` : `${date.getDate()}`;
    const month = date.getMonth() < 10 ? `0${date.getMonth() + 1}` : `${date.getMonth() + 1}`;

    return `${day}.${month}`;
}

export function dateToDay(date) {
    return daysMap[date.getDay()];
}

export function getDatesRange(date, amount) {
    return new Array(amount).fill(date).map((dt, index) => {
        let nextDay = new Date(dt);
        nextDay.setDate(dt.getDate() + index);
        nextDay.setHours(0, 0, 0, 0);
        return nextDay;
    });
}

export function getNextDate(date) {
    let nextDay = new Date(date);
    nextDay.setDate(date.getDate() + 1);
    return nextDay;
}

export function getPreviousDate(date) {
    let nextDay = new Date(date);
    nextDay.setDate(date.getDate() - 1);
    return nextDay;
}
