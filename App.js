import React, { useState } from "react";
import { AppLoading } from "expo";
import { Container, Text } from "native-base";
import * as Font from "expo-font";
import { Ionicons } from "@expo/vector-icons";
import Main from "./Components/Main";

export default function App() {
    const [isReady, setIsReady] = useState(false);

    async function load() {
        try {
            await Font.loadAsync({
                Roboto: require("native-base/Fonts/Roboto.ttf"),
                Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
                ...Ionicons.font,
            });
            setIsReady(true);
        } catch (err) {
            setIsReady(false);
            console.log(err);
        }
    }

    if (!isReady) {
        return <AppLoading startAsync={load} onFinish={() => setIsReady({ isReady: true })} onError={console.warn} />;
    }

    return <Main />;
}
